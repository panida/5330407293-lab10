package coe.lab10;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;

@WebServlet(name = "GetGeoIpWs", urlPatterns = {"/GetGeoIpWs"})
public class GetGeoIpWs extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
        String ipAddr = request.getParameter("ip");
        PrintWriter out = response.getWriter();
          try {
            // Create MessageFactory for create SOAPMessage
            MessageFactory factory = MessageFactory.newInstance();
            // Create SOAPMessage from MessageFactory
            SOAPMessage message = factory.createMessage();
            // get SOAPBody from SOAPMessage
            SOAPBody body = message.getSOAPBody();
            // Create SOAPFactory for prepare information of SOAP
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            // local name is GetGeoIP and prefix is ns 
            // namespace is http://www.webservicex.net/
            String prefix = "ns";
            String namespace = "http://www.webservicex.net/";
            Name opName = soapFactory.createName("GetGeoIP",prefix, namespace);
            // put opName in SOAPBody
            SOAPBodyElement opElem = body.addBodyElement(opName);
            // create SOAPElement of IpAddress
            SOAPElement ip = opElem.addChildElement(
                soapFactory.createName("IPAddress", prefix, namespace));
            ip.addTextNode(ipAddr); // get ip 
            // add SOAPAction
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", namespace + "GetGeoIP");
            // show SOAP Request
            // displayMessage(message, out);
            // create SOAPConnection for call web service and get result SOAPMessage
            SOAPConnection soapConn = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "http://www.webservicex.net/geoipservice.asmx";
            SOAPMessage resp = soapConn.call(message, endpoint);
            // show SOAP Response
            displayMessage(resp, out);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }
    
    private void displayMessage(SOAPMessage message, PrintWriter out) {
        try {
            TransformerFactory tFac = TransformerFactory.newInstance();
            Transformer transformer = tFac.newTransformer();
            Source src = message.getSOAPPart().getContent();
            StreamResult result = new StreamResult(out);
            transformer.transform(src, result);      
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    } 
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
