package coe.lab10;

import javax.xml.soap.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;



public class ServicePin {
    
    public static void main(String[] args) throws Exception {
        String pinId = "1100701508903";

        ServicePin ServicePIN = new ServicePin();
        ServicePIN.processRequest(pinId);
    }

    public void processRequest(String pinId) throws Exception {
        SOAPConnectionFactory factory =
            SOAPConnectionFactory.newInstance();
        SOAPConnection connection = factory.createConnection();
                
        MessageFactory msgfactory = MessageFactory.newInstance();
        SOAPMessage message = msgfactory.createMessage();

        SOAPHeader header = message.getSOAPHeader();
        
        SOAPBody body = message.getSOAPBody();
        header.detachNode();
        String prefix = "ns";
        String namespace = "https://rdws.rd.go.th/ServiceRD/CheckTINPINService";
        
        SOAPFactory soapFactory = SOAPFactory.newInstance();
        Name bodyName = soapFactory.createName("ServicePIN",
                prefix,namespace);
        SOAPBodyElement bodyElement = body.addBodyElement(bodyName); 

        Name user = soapFactory.createName("username",
                prefix,namespace);
        SOAPElement username = bodyElement.addChildElement(user);        
        username.addTextNode("anonymous");
        
        Name pass = soapFactory.createName("password",
                prefix,namespace);
        SOAPElement password = bodyElement.addChildElement(pass);        
        password.addTextNode("anonymous");
        
        Name pin = soapFactory.createName("PIN",
                prefix,namespace);
        SOAPElement PIN = bodyElement.addChildElement(pin);        
        PIN.addTextNode(pinId);
          
        // add SOAPAction
        MimeHeaders head = message.getMimeHeaders();
        head.addHeader("SOAPAction", namespace + "/ServicePIN");
        // show SOAP Request
     
        message.saveChanges();
        System.out.println("REQUEST:");  
        //Display Request Message
        displayMessage(message);
        
        System.out.println("\n\n");
        //add code below for trust x.509 ceritficate
        XTrustProvider.install();
        SOAPConnection conn =  SOAPConnectionFactory.newInstance().createConnection();
        SOAPMessage reply = conn.call(message,"https://rdws.rd.go.th/ServiceRD/CheckTINPINService.asmx");
        
        System.out.println("RESPONSE:");
        //Display Response Message
        displayMessage(reply);
    }

    public void displayMessage(SOAPMessage message) throws Exception {
        try {
            TransformerFactory tFac = TransformerFactory.newInstance();
            Transformer transformer = tFac.newTransformer();
            Source src = message.getSOAPPart().getContent();
            StreamResult result = new StreamResult(System.out);
            transformer.transform(src, result);
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
    }
}